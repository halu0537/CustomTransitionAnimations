//
//  ViewController.swift
//  CircularCustomTransitionAnimations
//
//  Created by Радим Гасанов on 20.12.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let transition = CircularTransition()
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        createButton()
        view.backgroundColor = .yellow
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }

}

//MARK: - Functions
extension ViewController {
    
    //MARK: - UI functions
    private func createButton() {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 180, height: 180)
        button.center.x = view.center.x
        button.center.x = view.center.y - 100
        button.layer.cornerRadius = 90
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
        button.backgroundColor = .lightGray
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitle("present", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        view.addSubview(button)
    }
    //MARK: - @objc functions
    @objc private func buttonDidTapped() {
        let vc = ViewControllerTwo()
        present(vc, animated: true, completion: nil)
    }
    //MARK: - another functions
}
//MARK: - UIViewControllerTransitioningDelegate
extension ViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("1 present")
        transition.transitionMode = .present
        transition.startingPoint = view.center
        transition.circleColor = .red
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("1 dismiss")
        transition.transitionMode = .dismiss
        transition.startingPoint = view.center
        transition.circleColor = .blue
        return transition
    }
}
