//
//  ViewControllerTwo.swift
//  CircularCustomTransitionAnimations
//
//  Created by Радим Гасанов on 20.12.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class ViewControllerTwo: UIViewController {
    let transition = CircularTransition()
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        createButton()
        view.backgroundColor = .green
        transitioningDelegate = self
        modalPresentationStyle = .custom
        //isModalInPresentation = true
    }
}

//MARK: - Functions
extension ViewControllerTwo {

    //MARK: - UI functions
    private func createButton() {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 180, height: 180)
        button.center = view.center
        button.layer.cornerRadius = 90
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
        button.backgroundColor = .lightGray
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitle("dismiss", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        view.addSubview(button)
    }
    //MARK: - @objc functions
    @objc private func buttonDidTapped() {
        dismiss(animated: true, completion: nil)
    }
    //MARK: - another functions
}
//MARK: - UIViewControllerTransitioningDelegate
extension ViewControllerTwo: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("2 present")
        transition.transitionMode = .present
        transition.startingPoint = view.center
        transition.circleColor = .red
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("2 dismiss")
        transition.transitionMode = .dismiss
        transition.startingPoint = view.center
        transition.circleColor = .blue
        return transition
    }
}
