//
//  SecondRouter.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

final class SecondRouterImpl: SecondRouter {

    weak var viewController: UIViewController?
    
    init() {}
    
    //MARK: - SecondRouter
    func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
