//
//  SecondAssembly.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class SecondAssembly {
    static func createModule() -> UIViewController {
        
        let view = SecondViewController()
        let state = SecondState()
        let router = SecondRouterImpl()
        router.viewController = view
        
        let presenter = SecondPresenterImpl(view: view, router: router, state: state)
        view.presenter = presenter
        return view
    }
}
