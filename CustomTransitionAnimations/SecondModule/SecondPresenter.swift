//
//  SecondPresenter.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation

class SecondPresenterImpl: SecondPresenter {

    let view: SecondView
    let router: SecondRouter
    var state: SecondState
    
    // MARK: - Init
    init(view: SecondView,
         router: SecondRouter,
         state: SecondState) {
        self.view = view
        self.router = router
        self.state = state
    }
    
    // MARK: - SecondPresenter
    func buttonDidTapped() {
        router.dismiss()
    }
}
