//
//  SecondViewController.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var presenter: SecondPresenter?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createButton()
        view.backgroundColor = .green
        //isModalInPresentation = true
    }
}
//MARK: - Functions
extension SecondViewController {

    //MARK: - UI functions
    private func createButton() {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 180, height: 80)
        button.center = view.center
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
        button.backgroundColor = .lightGray
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitle("dismiss to FirstModule", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        view.addSubview(button)
    }
    //MARK: - @objc functions
    @objc private func buttonDidTapped() {
        presenter?.buttonDidTapped()
    }
    //MARK: - another functions
}

//MARK: - FirstViewController
extension SecondViewController: SecondView {}

extension SecondViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("SecondViewController: UIViewControllerTransitioningDelegate present")
        let animationController = AnimationController(animationDuration: 3.5, animationType: .present)
        return animationController
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("SecondViewController: UIViewControllerTransitioningDelegate dismiss")
        let animationController = AnimationController(animationDuration: 4, animationType: .dismiss)
        return animationController
    }
}
