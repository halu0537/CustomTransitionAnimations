//
//  SecondProtocols.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation

protocol SecondPresenter: AnyObject {
    func buttonDidTapped()
}

protocol SecondView: AnyObject {}

protocol SecondRouter {
    func dismiss()
}
