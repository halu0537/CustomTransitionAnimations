//
//  AnimationController.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

// MARK: - AnimationController
class AnimationController: NSObject {
    
    private let animationDuration: Double
    private let animationType: AnimationType
    
    enum AnimationType {
        case present
        case dismiss
    }
    
    init(animationDuration: Double, animationType: AnimationType) {
        print("init \(animationDuration) \(animationType)")
        self.animationDuration = animationDuration
        self.animationType = animationType
    }
}

// MARK: - UIViewControllerAnimatedTransitioning
extension AnimationController: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        //print("transitionDuration \(TimeInterval(exactly: animationDuration))")
        return TimeInterval(exactly: animationDuration) ?? 5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        print("animateTransition")
        guard let toViewController = transitionContext.viewController(forKey: .to),
            let fromViewController = transitionContext.viewController(forKey: .from) else {
                transitionContext.completeTransition(false)
                return
        }
        
        print("switch animationType = \(animationType)")
        switch animationType {
        case .present:
            transitionContext.containerView.addSubview(toViewController.view)
            presentAnimation(width: transitionContext, viewToAnimate: toViewController.view)
        case .dismiss:
            //transitionContext.containerView.addSubview(toViewController.view)
            transitionContext.containerView.addSubview(fromViewController.view)
            dismissAnimation(width: transitionContext, viewToAnimate: fromViewController.view)
        }
    }
    
    private func presentAnimation(width transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView) {
        print("presentAnimation")
        viewToAnimate.clipsToBounds = true
        viewToAnimate.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.1, options: .curveEaseInOut, animations: {
            viewToAnimate.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }) { _ in
            transitionContext.completeTransition(true)
        }
    }
    
    private func dismissAnimation(width transitionContext: UIViewControllerContextTransitioning, viewToAnimate: UIView) {
        print("dismissAnimation")
//         viewToAnimate.clipsToBounds = true
//         viewToAnimate.transform = CGAffineTransform(scaleX: 0, y: 0)
        let duration = transitionDuration(using: transitionContext)
        let scaleDown = CGAffineTransform(scaleX: 0.3, y: 0.3)
        let moveOut = CGAffineTransform(translationX: -viewToAnimate.frame.width, y: 0)
        
        UIView.animateKeyframes(withDuration: duration, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.7) {
                viewToAnimate.transform = scaleDown
            }
            UIView.addKeyframe(withRelativeStartTime: 3.0/duration, relativeDuration: 1.0) {
                viewToAnimate.transform = scaleDown.concatenating(moveOut)
                viewToAnimate.alpha = 0
            }
        }) { _ in
            transitionContext.completeTransition(true)
        }
     }
}

//extension AnimationController: UIViewControllerInteractiveTransitioning {
//    func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
//        print("1234")
//    }
//}
