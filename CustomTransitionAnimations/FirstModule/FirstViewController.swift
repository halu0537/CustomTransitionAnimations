//
//  FirstViewController.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    var presenter: FirstPresenter?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createButton()
        view.backgroundColor = .yellow
    }

}

//MARK: - Functions
extension FirstViewController {

    //MARK: - UI functions
    private func createButton() {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 180, height: 80)
        button.center = view.center
        button.addTarget(self, action: #selector(buttonDidTapped), for: .touchUpInside)
        button.backgroundColor = .lightGray
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitle("navigate to SecondModule", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        view.addSubview(button)
    }
    //MARK: - @objc functions
    @objc private func buttonDidTapped() {
        presenter?.buttonDidTapped()
    }
    //MARK: - another functions
}

//MARK: - FirstView
extension FirstViewController: FirstView {}

//MARK: - UIViewControllerTransitioningDelegate
extension FirstViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("FirstViewController: UIViewControllerTransitioningDelegate present") //не работает
        let animationController = AnimationController(animationDuration: 3.5, animationType: .present)
        return animationController
    }
}
