//
//  FirstPresenter.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation

class FirstPresenterImpl: FirstPresenter {

    let view: FirstView
    let router: FirstRouter
    var state: FirstState
    
    // MARK: - Init
    init(view: FirstView,
         router: FirstRouter,
         state: FirstState) {
        self.view = view
        self.router = router
        self.state = state
    }
    
    // MARK: - FirstPresenter
    func buttonDidTapped() {
        router.navigateToSecondModule()
    }
}
