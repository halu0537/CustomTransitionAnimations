//
//  FirstRouter.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

final class FirstRouterImpl: FirstRouter {

    weak var viewController: UIViewController?
    
    init() {}
    
    //MARK: - FirstRouter
    func navigateToSecondModule() {
        let vc = SecondAssembly.createModule()
        viewController?.present(vc, animated: true, completion: nil)
    }
    
}
