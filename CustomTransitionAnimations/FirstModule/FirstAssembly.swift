//
//  FirstAssembly.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class FirstAssembly {
    static func createModule() -> UIViewController {
        
        let view = FirstViewController()
        let state = FirstState()
        let router = FirstRouterImpl()
        router.viewController = view
        
        let presenter = FirstPresenterImpl(view: view, router: router, state: state)
        view.presenter = presenter
        return view
    }
}
