//
//  FirstProtocols.swift
//  CustomTransitionAnimations
//
//  Created by Радим Гасанов on 18/12/2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import Foundation

protocol FirstPresenter: AnyObject {
    func buttonDidTapped()
}

protocol FirstView: AnyObject {}

protocol FirstRouter {
    func navigateToSecondModule()
}
