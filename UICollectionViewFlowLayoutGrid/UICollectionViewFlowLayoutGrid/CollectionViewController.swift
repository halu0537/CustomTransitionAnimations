//
//  CollectionViewController.swift
//  UICollectionViewFlowLayoutGrid
//
//  Created by Радим Гасанов on 23.12.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController {
    
    private let images = [UIImage(named: "1"),
                          UIImage(named: "2"),
                          UIImage(named: "3"),
                          UIImage(named: "4"),
                          UIImage(named: "5"),
                          UIImage(named: "6"),
                          UIImage(named: "7"),
                          UIImage(named: "8")]

    override func viewDidLoad() {
        super.viewDidLoad()
        if let layout = collectionView?.collectionViewLayout as? CustomCollectionViewFlowLayout {
          layout.delegate = self
        }
        self.collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.collectionView.backgroundColor = .lightGray
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.reuseID)
        self.collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.reuseID, for: indexPath) as? CollectionViewCell else {
            let defaultCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            defaultCell.backgroundColor = .orange
            return defaultCell
        }
        cell.backgroundColor = .blue
        cell.setImage(images[indexPath.item])
        return cell
    }
}

extension CollectionViewController: PinterestLayoutDelegate {
//    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
//    //print(images[indexPath.item]?.size.height)
//    return images[indexPath.item]?.size.height ?? 100
//  }
func collectionView(_ collectionView: UICollectionView, sizeForPhotoAtIndexPath indexPath: IndexPath) -> CGSize {
        return images[indexPath.item]?.size ?? CGSize(width: 100, height: 100)
    }
}
