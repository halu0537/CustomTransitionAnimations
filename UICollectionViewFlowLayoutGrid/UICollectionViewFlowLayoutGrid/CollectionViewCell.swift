//
//  CollectionViewCell.swift
//  UICollectionViewFlowLayoutGrid
//
//  Created by Радим Гасанов on 23.12.2019.
//  Copyright © 2019 Халу. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let reuseID = "CollectionViewCell"
    
    private var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
        
        imageView.backgroundColor = .red
        addSubview(imageView)
    }
    
    func setImage(_ image: UIImage?) {
        imageView.image = image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
